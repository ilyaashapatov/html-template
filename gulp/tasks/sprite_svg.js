var config = require('../config').spriteSvg,
    gulp = require('gulp'),
    plumber = require('gulp-plumber');

// js plugins
var sprites = require('gulp-svg-sprite'),
    svgmin = require('gulp-svgmin'),
    cheerio = require('gulp-cheerio'),
    replace = require('gulp-replace');

gulp.task('svg-sprite', function () {
    'use strict';

    gulp.src(config.src)
        .pipe(plumber())
        .pipe(svgmin(config.settings.svgmin))
        .pipe(cheerio(config.settings.cheerio))
        .pipe(replace('&gt;', '>'))
        .pipe(sprites(config.settings.sprites))
        .pipe(gulp.dest(config.dest));
});