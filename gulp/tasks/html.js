var config = require('../config').html,
    gulp = require('gulp'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    plumber = require('gulp-plumber'),
    rev = require('gulp-rev-hash'),

    gulpIf = require('gulp-if'),
    argv = require('yargs').argv;

// html plugins
var twig = require('gulp-twig');

// dev mode (если запускать сборку с флагом --dev то статика не будет минифицироваться)
// Пример: $ gulp --dev
// в данном случае ко всем линкованным файлам (js/css) будет добавляться хеш
gulp.task('html', function () {
    'use strict';
    return gulp.src(config.src)
        .pipe(plumber())
        .pipe(twig())
        .pipe(gulpIf(argv.dev, rev({assetsDir: './dist/'})))
        .pipe(gulp.dest(config.dest))
        .pipe(reload({stream: true}));
});