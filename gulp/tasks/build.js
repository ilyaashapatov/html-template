// var config = require('../config');
var gulp = require('gulp'),
    run = require('run-sequence');

gulp.task('build', function (done) { //['clean'],
    'use strict';

    run('bower', 'svg-sprite', 'png-sprite', 'js', 'images', 'css', 'html', 'files', done);
});